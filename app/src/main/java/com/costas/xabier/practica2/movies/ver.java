package com.costas.xabier.practica2.movies;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.costas.xabier.practica2.R;
import com.costas.xabier.practica2.model;


import java.util.ArrayList;



public class ver extends Activity {

    private ArrayAdapter<String> adapt;
    private ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver);

        this.listView = (ListView) findViewById(com.costas.xabier.practica2.R.id.listView);
        adapt = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);

        downloadMovieList list = new downloadMovieList();
        list.execute();
        listView.setAdapter(adapt);

       // AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
           // @Override
            //public void onItemClick(AdapterView<?> parent, View container, int position, long id) {
          //      goToMovieDetails(position);
         //   }

        //};
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               goToMovieDetails(position);
            }
        });
        //listView.setOnItemClickListener(itemClickListener);
    }

    public void goToMovieDetails(int position) {
        Intent intent = new Intent(this, movieDetails.class);
        position = position;
        intent.putExtra("position", position);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void onWerlcomeclicked() {
        Intent intent = new Intent(this, welcome.class);
        startActivity(intent);
    }

     class downloadMovieList extends AsyncTask<Void, Void, ArrayList<String>> {

        @Override
        protected ArrayList<String> doInBackground(Void... param) {

            try {
                ArrayList<String> array;
                array = model.movieList();
                return array;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<String> array) {
            super.onPostExecute(array);
            if (array != null) {
                for (String i : array) {
                    adapt.add(i);
                }
            } else {
                onWerlcomeclicked();
            }
        }
    }
}

