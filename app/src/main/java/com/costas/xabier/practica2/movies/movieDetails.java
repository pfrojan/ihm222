package com.costas.xabier.practica2.movies;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.costas.xabier.practica2.R;
import com.costas.xabier.practica2.model;

import java.util.ArrayList;

/**
 * Created by Xabier on 01/11/2014.
 */
public class movieDetails extends Activity{

    private int id;
    private SimpleAdapter adapt;
    private TextView  textView5, textView6, textView8, textView9, textView10, textView11, textView12;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infopeli);
        Intent intent = getIntent();
        id = intent.getIntExtra("position", id);
        this.textView5 = (TextView) findViewById(R.id.textView5);
        this.textView6 = (TextView) findViewById(R.id.textView6);
        this.textView8 = (TextView) findViewById(R.id.textView8);
        this.textView9 = (TextView) findViewById(R.id.textView9);
        this.textView10 = (TextView) findViewById(R.id.textView10);
        this.textView11 = (TextView) findViewById(R.id.textView11);
        this.textView12 = (TextView) findViewById(R.id.textView12);
        textView10.setMovementMethod(new ScrollingMovementMethod());
        downloadMovieInfo details = new downloadMovieInfo();
        details.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void onWerlcomeclicked() {
        Intent intent = new Intent(this, welcome.class);
        startActivity(intent);
    }

    public void onCommentsClicked(View view) {
        Intent intent = new Intent(this, commentsList.class);
        intent.putExtra("position", id);
        startActivity(intent);
    }

    class downloadMovieInfo extends AsyncTask<Void, Void, ArrayList<String>> {

        @Override
        protected ArrayList<String> doInBackground(Void... param) {

            try {
                ArrayList<String> array;
                array = model.movieInfo(id);
                return array;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<String> array) {
            super.onPostExecute(array);
            if (array != null) {
                textView5.setText(array.get(1));
                textView6.setText(array.get(3));
                textView8.setText(array.get(4));
                textView9.setText(array.get(0));
                textView10.setText(array.get(2));
                textView11.setText(array.get(6));
                textView12.setText(array.get(5));

            } else {
                onWerlcomeclicked();
            }
        }
    }
}
