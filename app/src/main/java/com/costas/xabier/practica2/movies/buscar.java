package com.costas.xabier.practica2.movies;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import com.costas.xabier.practica2.R;
import com.costas.xabier.practica2.model;

import java.util.ArrayList;



public class buscar extends Activity implements SearchView.OnQueryTextListener{

    private ArrayList<String> array;
    private ArrayAdapter<String> adapter;
    private SearchView searchView;
    private ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar);
        searchView = (SearchView) findViewById(R.id.queryBuscar);
        listView = (ListView) findViewById(R.id.listaPelis);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        downloadMovieList movieList = new downloadMovieList();
        movieList.execute();
        listView.setAdapter(adapter);
        listView.setTextFilterEnabled(true);

        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("Title");

        // AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        // @Override
        //public void onItemClick(AdapterView<?> parent, View container, int position, long id) {
        //      goToMovieDetails(position);
        //   }

        //};
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String aux = (String) parent.getItemAtPosition(parent.getSelectedItemPosition());

                int movie = array.indexOf(aux);
                Log.d("nop", String.valueOf(movie));
                goToMovieDetails(movie);
            }
        });
        //listView.setOnItemClickListener(itemClickListener);
    }

    public void goToMovieDetails(int movie) {
        Intent intent = new Intent(this, movieDetails.class);
        intent.putExtra("position", movie);

        startActivity(intent);
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        if (TextUtils.isEmpty(newText)) {
            listView.clearTextFilter();
        } else {
            listView.setFilterText(newText.toString());
        }
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void onWerlcomeclicked() {
        Intent intent = new Intent(this, welcome.class);
        startActivity(intent);
    }


    class downloadMovieList extends AsyncTask<Void, Void, ArrayList<String>> {

        @Override
        protected ArrayList<String> doInBackground(Void... param) {


            try {

                array = model.movieList();
                return array;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<String> array) {
            super.onPostExecute(array);
            if (array != null) {
                for (String i : array) {
                    adapter.add(i);
                }
            } else {
                onWerlcomeclicked();
            }

        }
    }
}

