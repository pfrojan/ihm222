package com.costas.xabier.practica2.movies;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.costas.xabier.practica2.R;
import com.costas.xabier.practica2.model;


public class newComent extends Activity {

    private int id;
    private String comment;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newcoment);
        Intent intent = getIntent();
        id = intent.getIntExtra("posicion", id);
        Log.d("no", String.valueOf(id));
    }

    public void onPostComentClicked(View view) {
        EditText editText = (EditText) findViewById(R.id.postComment);
        comment = editText.getText().toString();
        postComent post = new postComent();
        post.execute(comment);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.newcoment, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void goToCommentList() {
        Intent intent = new Intent(this, movieDetails.class);
        intent.putExtra("position", id);
        startActivity(intent);
    }

    public void onWerlcomeclicked() {
        Intent intent = new Intent(this, welcome.class);
        startActivity(intent);
    }

    class postComent extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... comment) {

            try {
                boolean success = model.postComent(String.valueOf(id), String.valueOf(comment));
                return success;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            if (success) {
                goToCommentList();
            } else {
                onWerlcomeclicked();
            }
        }
    }
}
