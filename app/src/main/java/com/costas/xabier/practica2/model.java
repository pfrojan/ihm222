package com.costas.xabier.practica2;

import android.net.http.AndroidHttpClient;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xabier on 21/10/2014.
 */
public class model {

    private static ArrayList<String> idList = new ArrayList<String>();
    private static ArrayList<String> idComentList = new ArrayList<String>();
    private String idMovie;

    public static JSONObject dataList(String url) throws Exception{
        AndroidHttpClient client = AndroidHttpClient.newInstance(GlobalNames.HTTP_USER_AGENT);
        HttpGet request = new HttpGet(url);
        HttpResponse response = null;
        JSONObject responseJason = null;

        response = client.execute(request);
        responseJason = new JSONObject(EntityUtils.toString(response.getEntity()));
        //System.out.print(responseJason.getString("result"));


        return responseJason;
    }

    public static ArrayList<String> movieList()throws Exception{

        try {
            JSONObject response = dataList("http://ipm-movie-database.herokuapp.com/movies");
            String success = response.getString("result");


            if (success.equals("success")) {
                JSONObject c;
                String movie;
                String id;
                ArrayList<String> array = new ArrayList<String>();
                JSONArray data = response.getJSONArray("data");

                for (int i = 0; i < data.length(); i++) {
                    c = data.getJSONObject(i);
                    movie = c.getString("title");
                    array.add(movie);
                    id = c.getString("id");
                    idList.add(id);
                }
                return array;
            }
            return null;
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static ArrayList<String> movieInfo(int id) throws Exception{
        try {
            String idMovie = idList.get(id);
            Log.d("nop", idMovie);
            JSONObject response = dataList("http://ipm-movie-database.herokuapp.com/movies/" + idMovie);
            String success = response.getString("result");

            if (success.equals("success")) {
                ArrayList<String> array = new ArrayList<String>();
                JSONObject data = (JSONObject)response.getJSONObject("data");

                array.add(data.getString("id"));
                array.add(data.getString("title"));
                array.add(data.getString("synopsis"));
                array.add(data.getString("year"));
                array.add(data.getString("category"));
                array.add(data.getString("username"));
                array.add(data.getString("email"));
                return array;
            }
            return null;
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<String> commentsList(int id)throws Exception{

        try {
            String idMovie = idList.get(id);
            JSONObject response = dataList("http://ipm-movie-database.herokuapp.com/movies/" + idMovie + "/comments");
            String success = response.getString("result");


            if (success.equals("success")) {
                JSONObject c;
                String comment;
                String userName;
                ArrayList<String> array = new ArrayList<String>();
                JSONArray data = response.getJSONArray("data");

                for (int i = 0; i < data.length(); i++) {
                    c = data.getJSONObject(i);
                    comment = c.getString("content");
                    userName = c.getString("username");
                    comment = comment + "\n" + userName;
                    array.add(comment);
                }
                return array;
            }
            return null;
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean postComent(String idMovie, String comment) throws Exception{
        AndroidHttpClient client = AndroidHttpClient.newInstance(GlobalNames.HTTP_USER_AGENT);
        HttpPost request = new HttpPost("http://ipm-movie-database.herokuapp.com/movies/" + idMovie + "/comments");
        HttpResponse response = null;
        JSONObject responseJason = null;
        Log.d("seeee",comment);
        Log.d("seeee",idMovie);
        // Add data
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("data", "content=" + comment));
        request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

        // Execute HTTP Post Request
        response = client.execute(request);

        responseJason = new JSONObject(EntityUtils.toString(response.getEntity()));
        String success = responseJason.getString("result");
        Log.d("nooooo",success);
        if (success.equals("success")) {
            return true;
        }
        return false;
    }
}