package com.costas.xabier.practica2.movies;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.costas.xabier.practica2.R;
import com.costas.xabier.practica2.model;

import java.util.ArrayList;

/**
 * Created by Xabier on 02/11/2014.
 */
public class commentsList extends Activity {
    private ArrayAdapter<String> adapt2;
    private ListView listView2;
    private int id;
    private ArrayList<String> array;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vercoments);
        Intent intent = getIntent();
        id = intent.getIntExtra("position", id);

        this.listView2 = (ListView) findViewById(com.costas.xabier.practica2.R.id.listView2);
        adapt2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        downloadCommentsList list2 = new downloadCommentsList();
        list2.execute();

        listView2.setAdapter(adapt2);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void onWerlcomeclicked() {
        Intent intent = new Intent(this, welcome.class);
        startActivity(intent);
    }

    public void goToNewComment(View view) {
        Intent intent = new Intent(this, newComent.class);
        intent.putExtra("position", id);
        startActivity(intent);
    }

    class downloadCommentsList extends AsyncTask<Void, Void, ArrayList<String>> {

        @Override
        protected ArrayList<String> doInBackground(Void... param) {

            try {

                array = model.commentsList(id);
                Log.d("nop", array.get(0));
                return array;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<String> array) {
            super.onPostExecute(array);
            if (array != null) {
                for (String i : array) {
                    Log.d("no", i);
                    adapt2.add(i);
                }
            } else {
                onWerlcomeclicked();
            }
        }
    }
}
